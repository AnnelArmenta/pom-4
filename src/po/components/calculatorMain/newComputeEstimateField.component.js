const baseComponent = require("../common/cloudPage.component.js");

class newComputeEstimateFields extends baseComponent{

    constructor(){
        super();
    }

    input(param){
        const selectInput = {
            numberOfInstances: '#input_100',
            }
        return $(selectInput[param]);
    }

    dropdown(param){
        const selectDropdown = {
            operatingSystem: '#select_value_label_92',
            machineFamily: '#select_value_label_94',
            series: '#select_value_label_95',
            machineType: '#select_value_label_96',
            localSSD: '#select_value_label_468',
            region: '#select_133',
            GPUModel: '#select_510', 
            numberOfGPUs:'#select_512',
            provisioningModel:'#select_value_label_93',
            committedUsage: '#select_140',         
        }
        return $(selectDropdown[param]);
    }

    get addGPUs(){
        return $('(//div[@class="md-label"])[3]');
    }
    get addEstimateButton(){
        return $('(//button[@class="md-raised md-primary cpc-button md-button md-ink-ripple"])[1]');
    }

}

module.exports = newComputeEstimateFields;