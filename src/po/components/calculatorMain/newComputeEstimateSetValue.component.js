const baseComponent = require("../common/cloudPage.component.js");

class newComputeEstimateSetValue extends baseComponent{

    constructor(){
        super();
    }

    operatingSystem(param){
        const selectValue = {
            free_Debian_Ubuntu_BYOL: '#select_option_102 div.md-text',
        }
        return $(selectValue[param]);
    }

    machineFamily(param){
        const selectValue = {
            General_Purpose: 'md-option[value="gp"]',
        }
        return $(selectValue[param]);
    }

    series(param){
        const selectValue = {
            N1: '#select_option_224',
        }
        return $(selectValue[param]);
    }

    machineType(param){
        const selectValue ={
            n1_standard_8: '#select_option_474',
        }
        return $(selectValue[param]);
    }   

    GPUModel(param){
        const selectValue ={
            NVIDIA_Tesla_V100: '#select_option_517',
        }
        return $(selectValue[param]);
    }

    numberOfGPUs(param){
        const selectValue ={
            li_1: '#select_option_520',
        }
        return $(selectValue[param]);
    }

    localSSD(param){
        const selectValue ={
            li_2x375GB: '#select_option_495',
        }
        return $(selectValue[param]);
    }

    region(param){
        const selectValue ={
            Netherlands: '#select_option_269',
        }
        return $(selectValue[param]);
    }

    provisioningModel(param){
        const selectModel = {
            regular: 'md-option[value="regular"]',
            }
        return $(selectModel[param]);
    }

    committedUsage(param){
        const selectUsage = {
            oneYear: '#select_option_138',
        }
        return $(selectUsage[param]);
    }

}

module.exports = newComputeEstimateSetValue;