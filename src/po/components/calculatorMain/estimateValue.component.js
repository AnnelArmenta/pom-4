const baseComponent = require("../common/cloudPage.component.js");

class estimateValue extends baseComponent{

    constructor(){
        super();
    }

    get resultBlock(){
        return $('#resultBlock');
    }
    get valueGoogle(){
        return $('h2[class="md-flex ng-binding ng-scope"]');
    }
}

module.exports=estimateValue;