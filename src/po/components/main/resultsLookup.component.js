const baseComponent = require("../common/cloudPage.component.js");

class resultsLookup extends baseComponent{

    constructor(){
        super();
    }

    item(param){
        const selectResult = {
            calculatorLegacy: 'a[href="https://cloud.google.com/products/calculator-legacy?hl=es-419"]',
            calculator: 'a[href="https://cloud.google.com/products/calculator"]'
        }
        return $(selectResult[param]);
    }
}

module.exports = resultsLookup;