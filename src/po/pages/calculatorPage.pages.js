const { CreateComputeEstimate, NewComputeEstimateField, NewComputeEstimateValue, ShadowGPU, EstimateValue, SendEmail, iFramesGoogle } = require("../components/index.js");
const pageOpener = require("./pageOpener.pages.js");

class CalculatorPage extends pageOpener{

    constructor(){
        super('https://cloud.google.com/products/calculator');
        this.createComputeEstimate = new CreateComputeEstimate();
        this.newComputeEstimateField = new NewComputeEstimateField();
        this.newComputeEstimateValue = new NewComputeEstimateValue();
        this.shadowGPU = new ShadowGPU();
        this.estimateValue= new EstimateValue();
        this.sendEmail= new SendEmail();
        this.iFramesGoogle = new iFramesGoogle();
    }
}

module.exports = CalculatorPage;